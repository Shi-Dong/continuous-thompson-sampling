\documentclass[11pt]{article}
\usepackage[toc,page]{appendix}
\usepackage{epsfig}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{esint}
\usepackage{algorithm, algorithmic}
\usepackage{color}
\usepackage{mathrsfs}
\usepackage[colorlinks,
            linkcolor=black,
            anchorcolor=black,
            citecolor=black
            ]{hyperref}

%\theoremstyle{plain}
%\newtheorem{assumption}{\protect\assumptionname}
\newtheorem{theorem}{Theorem}
\newtheorem{assumption}{Assumption}
\newtheorem{fact}{Fact}
\newtheorem{lem}{Lemma}
\newtheorem{defn}{Definition}
\newtheorem{cor}{Corollary}
\newtheorem{proposition}{Proposition}
\newtheorem{example}{Example}

% Definitions of handy macros can go here
\newcommand{\E}{\mathbb{E}}
\newcommand{\Prob}{\mathbb{P}}
\newcommand{\hist}{\F_{t}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\As}{\mathscr{A}}
\newcommand{\Y}{\mathcal{Y}}
\newcommand{\Ys}{\mathscr{Y}}
\newcommand{\DKL}{D}

\newcommand{\F}{\mathscr{F}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{A}}

\newcommand{\D}{\mathbb{D}} 

\def\C#1{\mathcal{#1}}
\def\RM#1{\mathrm{#1}}
\def\SF#1{\mathsf{#1}}
\def\FR#1{\mathfrak{#1}}
\def\B#1{\mathbf{#1}}
\def\BB#1{\mathbb{#1}}
\def\SCR#1{\mathscr{#1}}
\def\th{\theta}
\def\Th{\Theta}
\def\T{\top}
\def\e{\epsilon}
\def\h#1{\hat{#1}}
\def\t#1{\tilde{#1}}
\def\diam#1{\mathrm{diam}\left(#1\right)}
\def\d{\delta}
\def\a{\alpha}
\def\s{\sigma}
\def\z{\zeta} 
\def\p{\psi}
\def\add{\refstepcounter{count}}

% Definition of counter
\newcounter{count}
\renewcommand{\thecount}{\alph{count}} 


\newcommand{\argmin}{\mathop{\mathrm{argmin}}}
\newcommand{\argmax}{\mathop{\mathrm{argmax}}} 

\DeclareMathOperator*{\essinf}{{\rm ess}\!\inf}
\DeclareMathOperator*{\esssup}{{\rm ess}\!\sup}
\begin{document}
\title{An Information-Theoretic Analysis of\\ Thompson Sampling with General Action Spaces}
\maketitle
\section{Problem Formulation}
Let $\C{A} = \{a_1, \cdots, a_N\}$ be the set of actions, and $\C{P} = \{p^1,\cdots, p^M\}$ be the set of model parameters candidates. For each $p\in\C{P}$, let $\a(p)\in\C{A}$ denote the optimal action corresponding to model $p$. We assume that $\a$ is a surjection, i.e. every action is potentially optimal. The agent sequentially samples parameters $(p_t)_{t\in\BB{N}}$ from the parameter set $\C{P}$ and plays the action $A_t = \a(p_t)$. The agent then observes an outcome $Y_t\in\C{Y}$, where $\C{Y}$ is the set of outcomes, and
\[
Y_t = g(A_t, p^*, W_t).
\]
Here random variable $p^*$ denotes the true model parameter, $(W_t)_{t\in\BB{N}}$ represents an iid sequence of exogenous noise and $g$ is a known system function. With a slight abuse of notation, we use $Y_a=g(a, p^*, W)$ to denote the random outcome when action $a$ is played, where $W\overset{d}{=}W_i$.
A deterministic reward function $R:\C{Y}\mapsto [-\frac{1}{2},\frac{1}{2}]$ associates each outcome with a real-valued reward. 
We use $\mu(a,p)$ to denote the expected reward of playing action $a$ when the true parameter is $p$, i.e.
\[
\mu(a,p) = \E\left[R(Y_a) | p^*=p\right].
\]
Under this setting, the mapping $\a$ can be written as
\[
\a(p) = \argmax_{a\in\C{A}} \mu(a,p)\quad \forall p\in\C{P},
\]
where ties are broken arbitrarily if there are multiple optimal actions. Before sampling $p_t$, the agent is accessible to the history $\C{H}_{t-1} = (A_0, Y_0, \cdots, A_{t-1}, Y_{t-1})$. To characterize the randomness in decision making, we define {\it policy} as a function of history and random variable $\xi$, which is independent of $p^*$. The agent thereby samples according to
\[
p_t = \pi(\C{H}_{t-1}, \xi).
\]\par

To evaluate the performance of policy $\pi$, we introduce the {\it Bayesian cumulative regret}. Suppose we are evaluating $\pi$ with respect to a sequence of (possibly random) benchmark parameters $(p^*_t)_{t=1}^T$, the Bayesian cumulative regret is given by
\begin{align}
\label{eq:regretDef1}
{\rm BayesRegret}(T, \pi;(p^*_t)) 
& = 
\E \left[
\sum_{t=1}^T R(Y_{A^*_t}) - R(Y_{A_t})
\right]\nonumber\\
&= \E \left[
\sum_{t=1}^T R(Y_{\a(p^*_t)}) - R(Y_{\a(\pi(\C{H}_{t-1},\xi))})
\right]
,
\end{align}
where $A^*_t = \a(p^*_t)$ denotes the optimal action. The expectation is taken over the optimal actions $A^*_t$, the action sequence $(A_t)_{t=1}^T$ and outcomes $(Y_{A^*_t})_{t=1}^T, (Y_{A_t})_{t=1}^T$. As a shorthand we use
\[
{\rm BayesRegret}(T, \pi)
\]
to denote the Bayesian cumulative regret when $p^*$ is used as the benchmark parameter, i.e. $p^*_t=p^*,\ \forall 1\leq t\leq T$.

\begin{example}
{\bf (online linear regression)}
\end{example}

In particular, {\it Thompson sampling} is a policy that samples $p_t$ according to the posterior of $p^*$. Let $\pi^{\rm TS}$ denote the Thompson sampling policy, we have that
\begin{equation}
\label{eq:thompsonSampling}
\Prob\left(\pi^{\rm TS}(\C{H}_{t-1}, \xi)\in\cdot\right)
=
\Prob\left(p^*=\cdot | \C{H}_{t-1}\right)
\end{equation}

\section{Information Ratio}
The {\it information ratio}, first proposed in \cite{russo2016information}, quantizes the trade-off between exploration and exploitation. Here we adopt the more general definition similar to that in \cite{russo2014learning}. For any $\C{P}$-valued random variables $q, q'$ and random variable $x$, we define the information ratio as
\begin{equation}
\label{eq:infoRatioDef}
\Gamma(q, q',x) 
=
\frac{\E\left[R(Y_{\a(q')}) - R(Y_{\a(q)})\right]^2}{I\left(x; (\a(q), Y_{\a(q)})\right)}.
\end{equation}
We can interpret $x$ as the learning objective, and $q'$ as the benchmark parameter to which we compare the quality of $q$, which is the parameter we choose.
Under the setting of sequential decision making, let $\SCR{F}_t = \s(\a(q_0), Y_{\a(q_0)}, \cdots, \a(q_t), Y_{\a(q_t)})$ and let $q_t, q'_t:\Omega\mapsto\C{P}$ be $\SCR{F}_t$-measurable, the information ratio at stage $t$ is defined as
\begin{equation}
\label{eq:infoRatioDef2}
\Gamma(q_t, q'_t, x) 
=
\frac{\E_{t-1}\left[R(Y_{\a(q'_t)}) - R(Y_{\a(q_t)})\right]^2}{I_{t-1}\left(x; (\a(q_t), Y_{\a(q_t)})\right)},
\end{equation}
where the subscript $t-1$ denotes evaluation under base measure $\Prob(\cdot|\SCR{F}_{t-1})$.\par

Information ratio can also be defined for policies. Suppose that $x:\Omega\mapsto\C{P}$ is a random parameter that we desire to learn, $(q'_t)_{t=1}^T$ is a sequence of benchmark parameters, and the agent follows a policy $\pi$ up to a horizon $T$, then the information ratio of $\pi$ is defined as
\begin{equation}
\label{eq:infoRatioDef3}
\overline{\Gamma}(\pi; T, x, (q'_t)) = \max_{1\leq t\leq T}\sup_{\C{H}_{t-1}}\Gamma(\pi(\C{H}_{t-1},\xi), q'_t, x).
\end{equation}

Intuitively, if the agent follows a policy that has a low information ratio, at every stage she is expected to gain a relatively large amount of information about the true model parameter at the cost of a small regret, hence the final cumulative regret is likely to be small. The following theorem justifies this intuition. The proof is deferred to the appendix.
\begin{theorem}
\label{thm:infoRatio}
Suppose the agent follows policy $\pi$, then the Bayesian cumulative regret of the agent with respect to benchmark sequence $(q'_t)_{t=1}^T$ is bounded by
\begin{equation}
\label{eq:regretBoundInfo}
{\rm BayesRegret}(T, \pi; (q'_t)) \leq \sqrt{\overline{\Gamma}(\pi; T, x, (q'_t))\cdot H(x)T},
\end{equation}
where $x$ can be any learning objective and $H(x)$ is the entropy of $x$.
\end{theorem}

\section{Connection with Rate Distortion Theory}
\subsection{Lossy Channel and Rate Distortion}
In the first information ratio-based analysis of Thompson sampling \cite{russo2016information}, an upper bound $O\left(\sqrt{H(A^*)T}\right)$ was proposed without an y assumption on the action space $\C{A}$. When the prior of $A^*$ is a uniform distribution over $\C{A}$, the bound becomes $O\left(\sqrt{\log|\C{A}|\cdot T}\right)$. However, when $\C{A}$ is large and the outcomes of different actions are correlated, it is natural to expect that the structure of $\C{A}$ can be exploited to obtain a tighter bound, as is illustrated in Example \ref{ex:structure}. To this end, we incorporate the essence of {\it lossy compression} into our analysis.

\begin{example}
\label{ex:structure}
{\bf [TO BE DETERMINED]}
\end{example}

For any two non-random parameters $q_1, q_2\in\C{P}$, define the {\it distortion} between them as
\begin{equation}
\label{eq:distortionDef}
\ell(q_1,q_2) = \max_{q\in\C{P}} \big|\mu(\a(q_1), q) - \mu(\a(q_2), q)\big|.
\end{equation}
Let $q'_1, q'_2$ be two $\C{P}$-valued random variables, then the {\it average distortion} is defined as
\begin{equation}
\label{eq:averageDistortionDef}
\Delta(q'_1,q'_2) = \E\left[\ell(q'_1,q'_2)\right].
\end{equation}
A {\it lossy channel} $\C{C}$ can be characterized by the relation
\begin{equation}
\label{eq:channelDef}
\psi_{\rm out} = \C{C}(p_{\rm in}, \z),
\end{equation}
where $\C{C}$ is a deterministic function and $\z$ represents the idiosyncratic randomness of the channel. Here $p_{\rm in}\in\C{P}$ and $\p_{\rm out}$ is a random {\it summary statistic} taking values in set $\Psi$. We can think of $\p_{\rm out}$ as a summary of $p_{\rm in}$, which keeps some but not all information. The distortion between $\p_{\rm out}$ and $p_{\rm in}$ is defined as
\begin{equation}
\label{eq:summaryDistortionDef}
\t{\Delta}(\psi_{\rm out}, p_{\rm in}) = \min_{f:\Psi\mapsto\C{P}} \Delta(f(\psi_{\rm out}), p_{\rm in}),
\end{equation}
where the minimum is taken over all functions $f$ mapping $\Psi$ to $\C{P}$.
With a slight abuse of notation, let $$\t{\Delta}(\C{C})\equiv \t{\Delta}\left(\C{C}(p_{\rm in}, \z), p_{\rm in}\right)$$ be the channel distortion.
Intuitively, the larger the channel distortion, the less we can infer about $p_{\rm in}$ when we observe $\p_{\rm out}$. This intuition is made rigorous by the prestigious rate-distortion theory, which is covered in detail in Chapter 10 of \cite{cover2012elements}. Here we borrow the concept of {\it rate-distortion function} to facilitate our analysis.

For any $\C{P}$-valued random variable $q$, define the rate-distortion function $\C{R}(D;q)$ as
\begin{eqnarray}
\label{eq:rateDistortionDef}
\C{R}(D;q) = &\min& H(q')\\
&{\rm s.t.}& \Delta(q,q')\leq D,\nonumber
\end{eqnarray}
where the minimum is taken over all $\C{P}$-valued random variables $q'$. 
%It should be noted that for any $q$,
%\begin{equation}
%\label{eq:rateDistortionBound}
%\C{R}(D;q) \leq I(q;q) = H(q)\quad \forall D>0.
%\end{equation}

\subsection{Compressed Thompson Sampling}
In this section we propose a policy which we call {\it compressed Thompson sampling} (CTS). Different from the original Thompson sampling, this new policy samples from a compressed set of parameters, which naturally leads to smaller regret. At the end of this section, we draw a connection between the two policies and argue that the Bayesian cumulative regret of the original Thompson sampling is close to that of CTS.

Let $\e>0$ be our toleration of distortion, and $\C{C}$ be a channel such that
\begin{equation}
\label{eq:channelDistortionLimit}
\t{\Delta}(\C{C}) \leq \e.
\end{equation}
Let $\p^* = \C{C}(p^*, \z)$ be the output of channel upon receiving input $p^*$. From \eqref{eq:summaryDistortionDef} and \eqref{eq:channelDistortionLimit}, for each $t$ there exists a function $f_t:\Psi\mapsto \C{P}$ such that 
\begin{equation}
\label{eq:pStarDistortionBound}
\Delta(f_t(\psi^*), p^*) \leq \e
\end{equation}
Let $\t{p}_t^* = f_t(\p^*)$. Similarly, let $\p_t = \C{C}(p_t, \z'_t)$ be the output of channel upon receiving input $p_t$ and $\t{p}_t = f_t(\p_t)$, we have that
\begin{equation}
\label{eq:pTDistortionBound}
\Delta(\t{p}_t, p_t) \leq \e.
\end{equation}
Notice that we use two different random variables $\z$ and $\z'_t$ to emphasize that $\p^*$ and $\p_t$ are not correlated through the randomness of the channel.

In Thompson sampling, $p^*$ and $p_t$ are independent and identically distributed for all $1\leq t\leq T$, hence $\t{p}^*_t$ and $\t{p}_t$ are also independent and identically distributed for all $1\leq t\leq T$.
Let $f = (f_1,\cdots, f_T)$ denote the sequence of decoder functions. For fixed $f$ and $\e$, at each stage $t$, the policy of CTS($\e$), denoted as $\pi^{\RM{CTS}(\e)}_f$, samples the parameter $\t{p}_t$ and plays the action $\a(\t{p}_t)$, i.e. we have that
\[
\pi^{\RM{CTS}(\e)}_f \left( \C{H}_{t-1}, \xi_t \right) = f_t(\C{C}(p_t, \z_t)).
\]
The relation between Thompson sampling and CTS is illustrated in Figure \ref{fig:TS_and_CTS}. Example \ref{ex:partition} shows a specific instance of lossy channel, distortion and CTS.

\begin{figure}[htpb]
\centering
\includegraphics[width=4.5in]{TS_and_CTS.png}
\caption{TS and CTS. Two random variables connected by a teal dashed line are independent and identically distributed.}
\label{fig:TS_and_CTS}
\end{figure}
\begin{example}
\label{ex:partition}
{\bf (partition)}
\end{example}

%From definition we can see that at stage $t$ the CTS($\e$) policy samples from the set $f_t(\Psi)$. Since $\Psi$ is the set of all possible values of summary statistic $\psi$, it is expected that $\Psi$ has a much simpler structure comparing with the parameter set $\C{P}$, on which the original Thompson sampling policy samples. Further, the actions that CTS($\e$) chooses are closely coupled with those chosen by Thompson sampling via channel $\C{C}$. As a consequence, the regret of CTS($\e$) should not deviate much from the regret of Thompson sampling. In fact, we have the following.
The intuition behind CTS is that, although Thompson sampling policy may take a long time to identify the optimal model parameter $p^*$, it can quickly learn a satisficing parameter $\t{p}_t^*$, which yields only a small regret \cite{russo2017time}. The role of CTS is learning the satisficing parameter $\t{p}_t$ at every stage, hence the cumulative regret of CTS can be effectively bounded. As the following proposition shows, there exists a close connection between the performance of Thompson sampling and the performance of CTS, which we can leverage to bound the regret of Thompson sampling on large action (parameter) spaces.
\begin{proposition}
\label{prop:CTSandTS}
For any $\e > 0$, let $\C{C}_\e$ be a channel that satisfies \eqref{eq:channelDistortionLimit}, $f$ be any sequence of decoder functions that satisfies \eqref{eq:pStarDistortionBound} and $\pi^{\rm CTS}_f$ be the corresponding CTS policy, then
\[
{\rm BayesRegret}(T,\pi^{\rm TS}) \leq {\rm BayesRegret}(T,\pi^{\RM{CTS}(\e)}_f;(\t{p}^*_t)) + 2\e T.
\]
\end{proposition}


\section{General Regret Bound}
In this section we propose an upper bound for the Bayesian cumulative regret of Thompson sampling on the general bandit problems, based on rate-distortion theory. Before moving on, we need to specify our choice of decoder functions. Ideally, the decoder functions should be chosen such that if the agent sampled the decoded $\t{p}_t$ and played $\a(\t{p}_t)$ on stage $t$, the outcome she observes would be no more informative than the outcome of sampling $p_t$ and playing $\a(p_t)$. The following proposition guarantees the existence of such decoder functions.
\begin{proposition}
\label{prop:decoderExistence}
For any $\e>0$, there exists a sequence of decoder functions $\t{f}^\e = (\t{f}^\e_1, \cdots, \t{f}^\e_T)$ such that 
\begin{equation}
\label{eq:ftildeDef1}
\Delta(\t{f}^\e_t(\p^*), p^*)\leq\e \quad \forall 1\leq t\leq T,
\end{equation}
and
\begin{equation}
\label{eq:ftildeDef2}
I_{t-1}\left(\psi^*;(\a(\t{p}_t), Y_{\a(\t{p}_t)})\right)
\leq
I_{t-1}\left(\psi^*;(\a(p_t), Y_{\a(p_t)})\right)
\quad
\forall 1\leq t \leq T.
\end{equation}
\end{proposition}
Now we are ready to prove the main result.
\begin{theorem}
\label{thm:generalBound}
The Bayesian cumulative regret of Thompson sampling can be bounded by
\begin{equation}
\label{eq:generalBound}
{\rm BayesRegret}(T, \pi^{\rm TS})
\leq
\inf_{\e>0} \sqrt{\overline{\Gamma}\left(\pi^{\RM{CTS}(\e)}_{\t{f}^\e}; T, \p^*, (\t{p}^*_t)\right)\cdot \C{R}(\e;p^*)\cdot T} + 2 \e T.
\end{equation}
\end{theorem}
\proof
\qed

\section{Linear Bandits}
In this section we focus on linear bandit problems, which are a specific class of models in contextual bandit problems. We propose an upper bound of the Bayesian cumulative regret of Thompson sampling that depends on the dimension, rather than the size, of the action space.

In the linear bandit problems, the action and parameter spaces are compact subsets of the $d$-dimensional Euclidean space $\BB{R}^d$. Without loss of generality we assume that they are both contained in the $\ell_2$-unit ball $\C{B}_d = \{x\in\BB{R}^d: \|x\|_2\leq 1\}$. The mean reward is given by
\[
\mu(a, p) = \frac{1}{2}a^\T p.
\]
Further we also assume bandit feedback, i.e. $\Y = [-\frac{1}{2},\frac{1}{2}]$, and $R(Y_a)=Y_a$. Under this setting, using a similar line of analysis as in \cite{russo2016information}, we have that
\begin{proposition}
\label{prop:dOver2}
\begin{equation}
\label{eq:dOver2}
\overline{\Gamma}\left(\pi^{\RM{CTS}(\e)}_{\t{f}^\e}; T, \p^*, (\t{p}^*_t)\right) 
\leq
\frac{d}{2}
\end{equation}
\end{proposition}
We are now in place to state our main result.
\begin{theorem}
\label{thm:linearBandit}
For any linear bandit problems whose action and parameter spaces are $d$-dimensional, we have
\begin{equation}
\label{eq:linearBandit}
{\rm BayesRegret}(T, \pi^{\rm TS})
\leq d\sqrt{T\log\left( \frac{24\sqrt{2T}}{d} + 3\right)}.
\end{equation}
\end{theorem}
\proof
\qed
\section{Logistic Bandits}
\section{Conclusion}
\bibliography{bibliography}
\bibliographystyle{plain}
\newpage
\begin{appendices}
\section{Proof of Theorem \ref{thm:infoRatio}}
\newpage
\section{Proof of Proposition \ref{prop:CTSandTS}}
\newpage
\section{Proof of Proposition \ref{prop:decoderExistence}}
\newpage
\section{Proof of Proposition \ref{prop:dOver2}}
\end{appendices}
\end{document}
























