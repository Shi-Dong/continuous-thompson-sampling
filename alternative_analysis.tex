\documentclass[11pt]{article}

\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{esint}
\usepackage{algorithm, algorithmic}
\usepackage{color}
\usepackage{mathrsfs}
\usepackage[colorlinks,
            linkcolor=black,
            anchorcolor=black,
            citecolor=black
            ]{hyperref}

\usepackage{epsfig}



%\theoremstyle{plain}
%\newtheorem{assumption}{\protect\assumptionname}
\newtheorem{theorem}{Theorem}
\newtheorem{assumption}{Assumption}
\newtheorem{fact}{Fact}
\newtheorem{lem}{Lemma}
\newtheorem{defn}{Definition}
\newtheorem{cor}{Corollary}
\newtheorem{proposition}{Proposition}
\newtheorem{example}{Example}

% Definitions of handy macros can go here
\newcommand{\E}{\mathbb{E}}
\newcommand{\Prob}{\mathbb{P}}
\newcommand{\hist}{\F_{t}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\As}{\mathscr{A}}
\newcommand{\Y}{\mathcal{Y}}
\newcommand{\Ys}{\mathscr{Y}}
\newcommand{\DKL}{D_{\mathrm{KL}}}

\newcommand{\F}{\mathscr{F}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{A}}

\newcommand{\D}{\mathbb{D}} 

\usepackage{graphicx}
\newcommand{\indep}{\rotatebox[origin=c]{90}{$\models$}}


\def\C#1{\mathcal{#1}}
\def\RM#1{\mathrm{#1}}
\def\SF#1{\mathsf{#1}}
\def\FR#1{\mathfrak{#1}}
\def\B#1{\mathbf{#1}}
\def\BB#1{\mathbb{#1}}
\def\SCR#1{\mathscr{#1}}
\def\th{\theta}
\def\Th{\Theta}
\def\T{\top}
\def\e{\epsilon}
\def\h#1{\hat{#1}}
\def\t#1{\tilde{#1}}
\def\diam#1{\mathrm{diam}\left(#1\right)}
\def\d{\delta}
\def\a{\alpha}
\def\G{\Gamma}
\def\s{\sigma}
\def\diff{\RM{d}}
\def\add{\refstepcounter{count}}

% Definition of counter
\newcounter{count}
\renewcommand{\thecount}{\alph{count}} 


\newcommand{\argmin}{\mathop{\mathrm{argmin}}}
\newcommand{\argmax}{\mathop{\mathrm{argmax}}} 

\DeclareMathOperator*{\essinf}{{\rm ess}\!\inf}
\DeclareMathOperator*{\esssup}{{\rm ess}\!\sup}


\begin{document}
\title{Sketch of an Alternative Line of Analysis}

\maketitle


\section{Preliminaries}

Though our focus will be on sequential decision in a multi-period model, we will begin by defining some key concepts in the simpler context
of a single-period model.

\subsection{Single-Period Model}

We will define all random variables with respect to a probability space 
$(\Omega, \F, \Prob)$.  Let $\A$ be the set of possible actions and $\Y$ the set of outcomes.

Consider a model represented by a conditional distribution $p^*$, as illustrated in Figure \ref{fig:single_model}.
If the agent selects an action $A \in \A$, the outcome
$Y$ is distributed according to $\Prob(Y \in \diff y | \A, p^*) = p^*(\diff y | \A)$.
\begin{figure}[htpb]
\centering
\includegraphics[width=2.5in]{single_model.png}
\caption{From action to outcome.}
\label{fig:single_model}
\end{figure}

We take $p^*$ to itself be a random variable.  Let $\C{M}$ be a set of models that contains $p^*$, 
and let $\C{P}$ be the set of distributions over $\C{M}$.  Let $P \in \C{P}$ denote the distribution of $p^*$,
which we can think of as the agent's {\it belief distribution}.  In other words, $P$ is a probability distribution over $\C{M}$ such that $P(\diff p) = \Prob(p^* \in \diff p)$.
The agent selects the action $A$ according to a {\it policy} $\pi$ which is a distribution over $\A$.  In particular, 
$\Prob(A \in \diff a) = \pi(\diff a)$.  An optimal action $A^*$ is a random variable that maximizes
$\E[R(A^*) | p^*]$ for every $p^* \in \C{M}$.  
We assume that there exists a measurable optimal action and fix $A^*$ to be such an action.

\begin{example}
{\bf (linear model)}
\end{example}

\subsection{Regret and Information}

The agent experiences expected regret defined by $\E\left[R(A^*) - R(A) \right]$.  
Regret reflects the extent to which performance falls
short of optimal.  We are also interested in quantifying informativeness of feedback.  We will do so through 
the mutual information between the model and the realized action-observation pair:
$I(p^*; (A,Y))$.  This can be interpreted as the amount by which the observation reduces uncertainty about
$p^*$, measured in terms of entropy.  In particular, by the chain rule of entropy, 
$$H(p^* | (A,Y)) = H(p^*) - I(p^*; (A,Y)).$$

\subsection{Shuffles and Rate Distortion}

As the cardinality of the model set $\C{M}$ increases, entropy -- and therefore the amount to learn -- can grow unbounded.
However, it may suffice to learn an approximately optimal action.  As a framework for approximation, 
we introduce a new notion: an $\epsilon$-{\it shuffle} of the model $p^*$ is an identically distributed model $\tilde{p}$
for which $\E[R(A^*) - R(\tilde{A})] \leq \epsilon$,
where $\tilde{A}$ is sampled from $\Prob(A^* \in \cdot | p^* = \tilde{p})$.
The action $\tilde{A}$ can be thought of as an identically distributed approximation to $A^*$ which gives up no 
more than $\epsilon$ in expected reward.  This notion illustrated in Figure \ref{fig:shuffle}.
\begin{figure}[htpb]
\centering
\includegraphics[width=5in]{shuffle.png}
\caption{Shuffle.}
\label{fig:shuffle}
\end{figure}

Let $\C{S}_\epsilon$  denote the set of $\epsilon$-shuffles.  We define a {\it rate-distortion function}:
$$\rho(\epsilon) = \inf_{\tilde{p} \in \C{S}_\epsilon} I(p^*; \tilde{p}).$$

EXPLAIN INTUITION

\begin{example}
{\bf (partitioning a linear model)}
\end{example}


\subsection{Information Ratio}

We define the information ratio to be
\begin{equation}
\Gamma(\epsilon) = \sup_{\tilde{p} \in \C{S}_\epsilon} \frac{\E[R(\tilde{A}) - R(A)]^2}{I(\tilde{p}; (A, Y))}.
\end{equation}


\begin{example}
{\bf (Thompson sampling for a linear model)}
\end{example}



\section{Problem Settings}

We will define all random variables with respect to a filtered probability space 
$(\Omega, \F, (\F_t)_{t \in \BB{Z}_+}, \Prob)$.  As a convention, if a random variable is indexed by $t$,
it is $\F_t$-measureable.  For shorthand, we will use $\Prob_t(\cdot)$
and $\E_t[\cdot]$ to denote $\Prob(\cdot | \F_t)$ and $\E[\cdot|\F_t]$.\par
Let $\A$ be the set of actions and $\Y$ the set of outcomes.
At each time $t \in \BB{Z}_{++}$, the agent selects an action $A_t\in\A$ and observes an outcome $Y_t\in\Y$,
distributed according to $\Prob(Y_t \in \diff y | A_t, p^*) = p^*(\diff y | A_t)$, where $p^*$ is 
a conditional distribution over $\Y$ that governs the dependence of outcomes on actions.  
This setting is illustrated in Figure \ref{fig:model}.

\begin{figure}[htpb]
\centering
\includegraphics[width=2.5in]{model.png}
\caption{From action to outcome.}
\label{fig:model}
\end{figure}

We take $p^*$ to itself be a random variable.  This can be thought of as the ``true model'' an agent aims to learn.  Let $\C{M}$ be a deterministic set of models that contains $p^*$, 
and let $\C{P}$ be the set of distributions over $\C{M}$.  Let $P_t \in \C{P}$ denote the distribution of $p^*$ condsitioned on $\F_t$,
which we can think of as the agent's current {\it belief distribution}.  In other words, $P_t$ is a probability distribution over $\C{M}$ such that $P_t(\diff p) = \Prob_t(p^* \in \diff p)$.
We will assume that each action $A_t$ depends on history only through $P_{t-1}$.  In other words, 
$\Prob_{t-1}(A_t \in \diff a) = \Prob(A_t \in \diff a | P_{t-1})$.  We can think of actions as being selected by a {\it policy} $\pi$
which, given a belief distribution $P$, produces an action distribution $\pi_P$.  In particular,
$\Prob_{t-1}(A_t \in \diff a) = \pi_{P_{t-1}}(\diff a)$.

A reward function $R:\Y\mapsto\BB{R}$ associates a real-valued reward with each outcome. To normalize the reward, we assume the following.
\begin{assumption}
  \label{as:bounded}
  $\sup_{y\in\Y}R(y) - \inf_{y\in\Y}R(y) \leq 1$.
\end{assumption}
The optimal expected reward can be denoted by
$$R^* = \sup_{a\in\A} \int_{\Y} R(y) p^*(\diff y | a).$$
We assume that there exists a function $\a^*: \C{M}\mapsto\A$, such that for each $p\in\C{M}$, $\a^*(p)$ attains the above supremum when $p^*=p$. Let $A^*=\a^*(p^*)$ be the optimal action.
We also assume that this optimal action is $\F$-measurable.
In addition we assume that if the true model is known then the policy selects the optimal action.  In other words,
if $P_t(\diff p) = 1$ for all $\diff p \supseteq p^*$ then $A_t = A^*$; note that this implies that
$\pi_P$ assigns all probability to $A^*$ if $P$ assigns all probability to $p^*$.

To evaluate the performance of a policy $\pi$, we define the {\it cumulative regret} over $T$ time periods by
$${\rm BayesRegret}(T;\pi) = \E\left[\sum_{t=1}^T (R^* - R_t)\right],$$ where the actions $A_1,\cdots, A_T$ are selected according to $\pi$. Intuitively, the cumulative regret measures the difference between the performance of policy $\pi$ and the performance of the optimal policy, which plays the optimal action in each period.

\section{Rate-Distortion and Information Ratio}
\label{sec:infoRatio}
We build upon the notion of {\it lossy compression} the information ratio and the rate-distortion function, as is illustrated in Figure \ref{fig:compression}.

\begin{figure}[htpb]
\centering
\includegraphics[width=5in]{encoder.png}
\caption{Lossy compression.}
\label{fig:compression}
\end{figure}

Consider an encoder that, given $p^*$, generates a summary statistic $\psi^*$ that captures some but not all information conveyed by $p^*$.
This encoder can be represented in terms of a conditional distribution $q^e$, for which $\Prob(\psi^* \in \diff \psi | p^*) = q^e(\diff \psi|p^*)$.
A decoder $q^d$ then produces a proxy $\tilde{p}_{t-1} \in \C{M}$ of $p^*$ from the summary statistic $\psi^*$ and the belief $P_{t-1}$. 
We use $\tilde{P}_{t-1} \in \C{P}$ to denote the belief distribution of $\tilde{p}_{t-1}$.

The optimal action $A^*$ can be generated
by maximizing expected reward given $p^*$.  At time $t$, an approximation of the optimal action, which we denote as $\tilde{A}^*_t$, can be generated by maximizing expected reward conditioned on $p^* = \tilde{p}_t$.  The policy generates an action $A_t$ by sampling from $\pi_{P_{t-1}}$.
Similarly, the same policy can be applied to produce a suboptimal action $\tilde{A}_t$ by sampling from $\pi_{\tilde{P}_{t-1}}$. Let
$$\tilde{R}^*_t = \int_{\Y} R(y) p^*(\diff y|\tilde{A}^*_t) \qquad \text{and} \qquad \tilde{R}_t = \int_{\Y} R(y) p^*(\diff y|\tilde{A}_t).$$
Note that these quantities represent expected rewards, conditioned on $p^*$, generated by actions 
$\tilde{A}^*_t$ and $\tilde{A}_t$, respectively.
We define the {\it distortion} induced by an encoder-decoder pair $(q^e, q^d)$ given belief $P$ to be
\begin{equation}
  \label{eq:defDistortion}
  \Delta_P(q^e, q^d) =  \Big|\E[R^* - R_t | P_{t-1} = P] - \E_{t-1}[\tilde{R}^*_t - \tilde{R}_t | P_{t-1} = P]\Big|.
\end{equation}


For the purposes of our analysis, we will always take the decoder $q^d_{t-1}$ to be that which yields the least
informative feedback subject to a distortion constraint $\d$.  In particular, given $q^e$, $q^d_{t-1}$ is taken to be the decoder that attains the infimum [NOTE: assumption needed] in
\begin{equation}
  \label{eq:infimum}
  \inf_{q^d_{t-1}: \Delta_{t-1}(q^e, q^d_{t-1}) \leq \d} I_{t-1}(\psi^*; (\tilde{A}_t, \tilde{Y}_t)),
\end{equation}
where $\Prob(\tilde{Y}_t \in \diff y | p^*, \tilde{A}_t) = p^*(\diff y|\tilde{A}_t)$, and $\t{Y}_t$ is the outcome associated with the action $\t{A}_t$. Notice that, since $\t{A}_t$ is independent of $\psi^*$ conditioned on $\F_{t-1}$, we have
\[
  I_{t-1}(\psi^*;(\t{A}_t, \t{Y}_t))
  = I_{t-1}(\psi^*; \t{A}_t) + I_{t-1}(\psi^*; \t{Y}_t | \t{A}_t)
  = I_{t-1}(\psi^*; \t{Y}_t | \t{A}_t).
\]
By choosing $q_{t-1}^d$ that attains the infimum in \eqref{eq:infimum}, we can guarantee that for any random action $A$ that is independent of $\psi^*$ conditioned on $\F_{t-1}$, let $Y$ be its outcome, there is
\begin{align}
  \label{eq:infimumResult}
  I_{t-1}(\psi^*;(A,Y))
  &= I_{t-1}(\psi^*;Y|A)\nonumber\\
  &\geq I_{t-1}(\psi^*; \t{Y}_t|\t{A}_t)\nonumber\\
  &= I_{t-1}(\psi^*;(\t{A}_t, \t{Y}_t)).
\end{align}
With an understanding that the decoder
is always selected in this manner, and with some abuse of notation, we can express distortion as a function
of only the encoder: $\Delta_{t-1}(q^e)$.  We also define an outcome-independent and time-independent notion of distortion:
\begin{equation}
  \label{eq:defDistortion2}
  \Delta(q^e) = \esssup_{\omega \in \Omega} \sup_{t\in\{1,\cdots,T\}} \Delta_{t-1}(q^e).
\end{equation}
Finally, we define a rate-distortion function:
\begin{equation}
  \label{eq:defRDF}
  \rho(\e) = \inf_{q^e: \Delta(q^e) \leq \e} I(p^*;  \psi^*).
\end{equation}

We define the information ratio to be
\begin{equation}
\Gamma = \sup_{P, q^e, q^d} \frac{\E[\tilde{R}^*_t - \tilde{R}_t | P_t=P]^2}{I(\psi^*; (\tilde{A}_t, \tilde{Y}_t) |P_t = P)}.
\end{equation}





\section{General Regret Bound}
\label{sec:regBound}
The regret bound proposed in the following theorem can be applied to any general policy $\pi$.
\begin{theorem}
\label{thm:general_bound}
Let $\pi$ be a policy that satisfies the assumptions, and $\G$ be the information ratio of $\pi$ defined in \eqref{eq:defInfoRatio}. Then for all $T\in \Z_{++}$,  
\begin{equation}
  \label{eq:generalRegBound}
  {\rm BayesRegret}(T;\pi) \leq  \inf_{\epsilon \geq 0} \left(\sqrt{\Gamma \rho(\epsilon) T} + \epsilon T\right).
\end{equation}
\end{theorem}
\proof Fix $\e>0$, and consider an arbitrary encoder $q^e$ such that $\Delta(q^e)\leq\e$, we have that
\begin{eqnarray}
  {\rm BayesRegret}(T;\pi)
  &=& \E  \sum_{t=1}^T\E_{t-1}\Big[ R^*-R_t \Big]\nonumber \\
  &\overset{(\add\label{1}\thecount)}{\leq}& \E\sum_{t=1}^T\E_{t-1}\left[ \t{R}_t^* - \t{R}_t \right] + \e T\nonumber \\
  &\overset{(\add\label{2}\thecount)}{\leq}& \E\sum_{t=1}^T\sqrt{\G_{t-1}\cdot I_{t-1}\Big(\psi^*;(\t{A}_t, \t{Y}_t)\Big)} + \e T\nonumber \\
  &\leq& \E\sum_{t=1}^T\sqrt{\G\cdot I_{t-1}\Big(\psi^*;(\t{A}_t, \t{Y}_t)\Big)} + \e T\nonumber \\
  &\overset{(\add\label{3}\thecount)}{\leq}&  \sqrt{\G T} \cdot \E \sqrt{\sum_{t=1}^T I_{t-1}\Big(\psi^*;(\t{A}_t, \t{Y}_t)\Big)} +\e T \nonumber \\
  &\overset{(\add\label{4}\thecount)}{\leq}&  \sqrt{\G T} \cdot  \sqrt{\sum_{t=1}^T \E I_{t-1}\Big(\psi^*;(\t{A}_t, \t{Y}_t)\Big)} +\e T\nonumber \\
  &\overset{(\add\label{a}\thecount)}{\leq}&  \sqrt{\G T} \cdot  \sqrt{\sum_{t=1}^T \E I_{t-1}\Big(\psi^*;(A_t, Y_t)\Big)} +\e T\nonumber \\
  &=&  \sqrt{\G T} \cdot  \sqrt{\sum_{t=1}^T  I\Big(\psi^*;(A_t, Y_t)\big|(A_1, Y_1, \cdots, A_{t-1}, Y_{t-1}) \Big)} +\e T\nonumber \\
  &\overset{(\add\label{5}\thecount)}{=}&  \sqrt{\G T} \cdot  \sqrt{I\Big(\psi^*;(A_1, Y_1,\cdots,A_T, Y_T)\Big)} +\e T\nonumber \\
  &\overset{(\add\label{6}\thecount)}{\leq}&  \sqrt{\G T} \cdot  \sqrt{I(\psi^*;p^*)} +\e T,\nonumber
\end{eqnarray}
where $(\ref{1})$ follows from the definition of distortion in \eqref{eq:defDistortion} and \eqref{eq:defDistortion2}; $(\ref{2})$ follows from the definition of information ratio in \eqref{eq:defInfoRatio}; $(\ref{3})$ comes from Cauchy-Schwartz inequality; $(\ref{4})$ comes from Jensen's inequality; $(\ref{a})$ comes from \eqref{eq:infimumResult}; $(\ref{5})$ is the chain rule of mutual information, and $(\ref{6})$ results from data processing inequality and the fact that $\psi^*$ is independent of $(A_1, Y_1,\cdots,A_T, Y_T)$ conditioned on $p^*$. By taking the infimum over $\e$ and the encoder $q^e$, we arrive at \eqref{eq:generalRegBound}. \qed

\noindent[NOTE: it seems that the proof of Theorem \ref{thm:general_bound} doesn't require the discreteness of $\psi^*$.]


%\section{Regret Bound for Thompson Sampling}
%\label{sec:ts}
%Thompson sampling is an algorithm that sequentially samples actions from the posterior of the optimal action. Let $\pi^{\rm TS}$ denote the policy of Thompson sampling, we have that $\pi^{\rm TS}_{P_{t-1}}(\diff a) = P_{t-1}(\diff a)$. The following proposition, which is a similar version of Proposition 2 in \cite{russo2016information}, is useful to our analysis of Thompson sampling.
%
%\begin{proposition}
%  \label{prop:ts}
%  Let $q^*$ and $\h{q}$ be two independent and identically distributed $\C{M}$-valued random variables, and let $B^* = \a^*(q^*)$, $\h{B} = \a^*(\h{q})$ be their corresponding optimal actions. For any action $a\in\C{A}$, let $Z_a\in\C{Y}$ denote the outcome of action $a$ under model $p^*$, i.e. $\Prob(Z_a\in\diff y) = p^*(\diff y| a)$, where $p^*$ is independent of $\h{q}$. We have the followings:
%\begin{equation}
%    \label{eq:decomposeI}
%      I\Big(B^*; (\h{B}, Z_{\h{B}})\Big) = \sum_{b, b'\in\A} \Prob\left( B^* = b \right)\Prob\left( B^* = b'\right)\cdot
%    D_{\rm KL}\Big( P\big(Z_{b}|B^* = b\big) \big\| P\big(Z_b)\big) \Big),
%  \end{equation}
%  and
%  \begin{equation}
%    \label{eq:decomposeE}
%    \E\left[ R(Z_{B^*}) - R(Z_{\h{B}}) \right]
%    =
%    \sum_{b\in\A} \Prob\left( B^*=b \right)\cdot \\
%    \Big(
%    \E\left[ R(Z_b)| B^*=b \right] - \E\left[ R(Z_b) \right]
%    \Big),
%  \end{equation}
%  where $P(\cdot)$ denotes the distribution of a random variable, and $D_{\rm KL}$ is the Kullback-Leibler divergence.
%\end{proposition}
%\proof Notice that $B^*$ and $\h{B}$ are $\C{A}$-valued random variables and are independent and identically distributed, there is 
%\begin{eqnarray*}
%  I\Big(B^*;(\h{B}, Z_{\h{B}})\Big)
%  &\overset{(\add\label{14}\thecount)}{=}& I\Big(B^*;\h{B}\Big) + I\Big(B^*;Z_{\h{B}} \big| \h{B}\Big)\\
%  &\overset{(\add\label{15}\thecount)}{=}& I\Big(B^*;Z_{\h{B}} \big| \h{B}\Big)\\
%  &=& \sum_{b\in\A} I\Big(B^*;Z_{\h{B}}\big|\h{B} = b\Big) \Prob\Big(\h{B} = b\Big)\\
%  &=& \sum_{b\in\A} I\Big(B^*;Z_{b}\Big) \Prob\Big(\h{B} = b\Big)\\
%  &=& \sum_{b\in\A}\sum_{b'\in\A} \Prob\Big(\h{B} = b\Big)\Prob\Big(B^* = b'\Big)D_{\rm KL}\Big( P\big(Z_b|B^*=b\big) \big\| P\big(Z_b\big)\Big) \\
%  &\overset{(\add\label{16}\thecount)}{=}& \sum_{b,b'\in\A}\Prob\Big(B^* = b\Big)\Prob\Big(B^* = b'\Big)D_{\rm KL}\Big( P\big(Z_b|B^*=b\big) \big\| P\big(Z_b\big)\Big),
%\end{eqnarray*}
%where $(\ref{14})$ comes from the chain rule for mutual information, $(\ref{15})$ comes from the independence between $B^*$ and $\h{B}$, and $(\ref{16})$ follows from that $B^*$ and $\h{B}$ are identically distributed. Thus \eqref{eq:decomposeI} is justified. To show \eqref{eq:decomposeE}, we have
%\begin{eqnarray*}
%  \E\left[ R(Z_{B^*}) - R(Z_{\h{B}}) \right]
%  &\overset{(\add\label{17}\thecount)}{=}& \sum_{b\in\A} \Prob\Big(B^*=b\Big)\Big(\E\Big[ R(Z_{B^*}) \big| B^*=b \Big] - \E\left[ R(Z_{\h{B}}) \big| \h{B}=b \right] \Big)\\
%  &=& \sum_{b\in\A} \Prob\Big(B^*=b\Big)\Big(\E\left[ R(Z_{b}) | B^*=b \right] - \E\left[ R(Z_{b})  \right] \Big),
%\end{eqnarray*}
%where $(\ref{17})$ results from that $B^*$ and $\h{B}$ are identically distributed.  
%\qed\\
%
%Next we introduce the regret bound for Thompson Sampling on linear bandit problems. Let $\C{B}_d = \{x\in\BB{R}^d: \|x\|_2\leq 1\}$ denote the $d$-dimensional unit ball with respect to the Euclidean norm. The linear bandit is a class of models that satisfy the following assumption.
%
%\begin{assumption}
%  \label{as:linear}
%  There exists $d \in \Z_{++}$ such that $\A \subset \mathcal{B}_d$, $\Y\subset\BB{R}$, $\C{M} = \C{B}_d$, $R$ is the identity function, and for all $p \in \C{M}$ 
%there is $$\E[R(Z_a)] = p^\top a,\ \forall a\in\C{A},$$ where $Z_a\in\Y$ satisfies $\Prob(Z_a\in\diff y) = p(\diff y|a)$.
%\end{assumption}
%Before moving on, we introduce two facts that facilitate our analysis. We refer readers to \cite{russo2016information} for the proofs.
%\begin{fact}
%  \label{fact:Pinsker}
%  {\rm (Pinsker's inequality, Fact 9 of \cite{russo2016information})}
%  Let $\mu_1$ and $\mu_2$ be two probability measures on measurable space $(\Omega, \SCR{H})$ such that $\mu_1\ll\mu_2$. Suppose $X:\Omega\mapsto\C{X}$ is a random variable and $g:\C{X}\mapsto\BB{R}$ is a function such that $\sup g - \inf g\leq 1$, then
%  \[
%    \E_{\mu_1}\left[ g(X) \right] - \E_{\mu_2}\left[ g(X) \right] \leq \sqrt{\frac{1}{2}D_{\rm KL}(\mu_1\|\mu_2)},
%  \]
%  where $\E_{\mu}$ denotes the expectation of a random variable evaluated under probability measure $\mu$.
%\end{fact}
%\begin{fact}
%  \label{fact:matrix}
%  {\rm (Fact 10 of \cite{russo2016information})}
%  For any real-valued square matrix $U$, there is
%  \[
%    \RM{Trace}(U) \leq \sqrt{\RM{Rank}(U)}\cdot\|U\|_{\rm F},
%  \]
%  where $\|\cdot\|_{\rm F}$ denotes the Frobenius norm.
%\end{fact}
%The next result upper bounds the information ratio of Thompson sampling on linear bandit problems.
%\begin{theorem}
%\label{thm:ubir}
%Under Assumptions \ref{as:bounded} and \ref{as:linear} there is $\G(\pi^{\rm TS}) \leq d/2$.
%\end{theorem}
%\proof
%Let $\A= \{a_1, \cdots, a_{|\A|}\}$.  Let $q^*, \hat{q}\in\C{P}$ be independent and identically distributed $\C{M}$-valued random variables. For each $a\in\A$, let $Z_a\in\Y$ be sampled according to $\Prob(Z_a\in\diff y) = p^*(\diff y|a)$, where $p^*$ is independent of $\h{q}$.
%
%For $i \in \{1,\ldots,|\A|\}$, let $p_i = \Prob(\alpha^*(q^*) = a_i) = \Prob(\alpha^*(\h{q}) = a_i)$.
%Define a matrix $M \in \BB{R}^{|\A| \times |\A|}$ with elements 
%$$M_{i,j} = \sqrt{p_ip_j}\cdot \Big( \E[R(Z_{a_i}) | \alpha^*(q^*) = a_j] - \E[R(Z_{a_i})] \Big).$$
%Then, by Proposition \ref{prop:ts}, there is
%$$\E[R(Z_{\alpha^*(q^*)}) - R(Z_{\alpha^*(\hat{q})})] 
%= \sum_{i=1}^{|\A|} p_i \Big(\E[R(Z_{a_i}) | \alpha^*(q^*) = a_i] - \E[R(Z_{a_i})]\Big) =  {\rm Trace}(M),$$
%and
%\begin{eqnarray*}
%I\Big(q^*; (\alpha^*(\hat{q}), Z_{\alpha^*(\hat{q})})\Big)
%  &\overset{(\add\label{11}\thecount)}{\geq}& I\Big(\alpha^*(q^*); (\alpha^*(\hat{q}), Z_{\alpha^*(\hat{q})})\Big) \\
%&\overset{(\add\label{12}\thecount)}{=}& \sum_{i,j=1}^{|\t{\A}|} p_i p_j D_{\rm KL}\Big(\Prob(Z_{a_i} | \alpha^*(q^*) = a_j) \big\| \Prob(Z_{a_i})\Big) \\
%&\overset{(\add\label{13}\thecount)}{\geq}& 2 \sum_{i,j=1}^{|\t{\A}|} p_i p_j \Big(\E[R(Z_{a_i}) | \alpha^*(q^*) =  a_j] - \E[R(Z_{a_i})]\Big)^2 \\
%&=& 2 \|M\|_{\rm F}^2,
%\end{eqnarray*}
%where $(\ref{11})$ results from data-processing inequality; $(\ref{12})$ follows from Proposition \ref{prop:ts}; $(\ref{13})$ follows from Assumption \ref{as:bounded} and Fact \ref{fact:Pinsker}. Considering Fact \ref{fact:matrix}, it then follows that
%\begin{equation}
%  \label{eq:rank}
%  \frac{\E[R(Z_{\alpha^*(q^*)}) - R(Z_{\alpha^*(\hat{q})})]^2}{I\Big(q^*; (\alpha^*(\hat{q}), Z_{\alpha^*(\hat{q})})\Big)} \leq \frac{{\rm Trace}(M)^2}{2 \|M\|_{\rm F}^2} \leq \frac{{\rm Rank}(M)}{2}.
%\end{equation}
%Since the distribution of $q^*$ and $\h{q}$ is arbitrary, for fixed $t$ we can let this distribution be the belief distribution $\t{P}_{t-1}$. Thus we have
%\begin{equation}
%  \label{eq:Gamma}
%  \frac{\E_{t-1}[\t{R}_t^* - \t{R}_t]^2}{I_{t-1}(\t{p}_{t-1};(\t{A}_t, \t{Y}_t))} \leq \frac{{\rm Rank}(M)}{2}.
%\end{equation}
%Notice that since $\t{p}_{t-1}$ is the output of decoder $q^d_{t-1}$ and is thereby independent of $(\t{A}_t, \t{Y}_t)$ conditioned on $\psi^*$ and $\F_{t-1}$. By data-processing inequality we have
%\begin{equation}
%  \label{eq:dataprocessing}
%  I_{t-1}(\t{p}_{t-1};(\t{A}_t, \t{Y}_t)) \leq I_{t-1}(\psi^*;(\t{A}_t, \t{Y}_t)).
%\end{equation}
%Combining \eqref{eq:Gamma} and \eqref{eq:dataprocessing}, and taking the supremum over $t$ and $q^e$, we arrive at
%\begin{equation}
%  \label{eq:GammaBound}
%  \G(\pi^{\rm TS})\leq\frac{\RM{Rank}(M)}{2}.
%\end{equation}\par
%We will now show that ${\rm Rank}(M) \leq d$.  Let
%$$\mu = \E[q^*] \qquad \text{and} \qquad \mu^i = \E[q^* | \alpha^*(q^*) = a_i].$$
%Then, by the tower property and linearity of the expectation operator,
%\begin{align}
%  M_{i,j}
%  &= \sqrt{p_ip_j}\cdot\Big( \E[R(Z_{a_i}) | \alpha^*(q^*) = a_j] - \E[R(Z_{a_i})] \Big) \nonumber\\
%  &= \sqrt{p_ip_j}\cdot\Big( \E\Big[ \E[R(Z_{a_i}) | \alpha^*(q^*) = a_j, q^*] \Big] - \E\Big[ \E[R(Z_{a_i})| q^*] \Big] \Big)\nonumber\\
%  &= \sqrt{p_ip_j}\cdot\Big( \E\Big[ (q^*)^\T a_i  \big| \alpha^*(q^*) = a_j \Big] - \E\Big[(q^*)^\T a_i\Big] \Big)\nonumber\\
%  &= \sqrt{p_i p_j}\cdot \big((\mu^j - \mu)^\top a_i\big)\nonumber.
%\end{align}
%Therefore
%$$M = 
%\left[\begin{array}{c}
%\sqrt{p_1} (\mu^1 - \mu)^\top \\
%\vdots \\
%\sqrt{p_{|\t{\A}|}} (\mu^{|\t{\A}|} - \mu)^\top
%\end{array}\right]
%\left[\sqrt{p_1} a_1 \cdots \sqrt{p_{|\t{\A}|}} a_{|\t{\A}|}\right].$$
%Hence, $M$ is a product of an $|\t{\A}| \times d$ matrix and a $d \times |\t{\A}|$ matrix, which has rank at most $d$.
%\qed\\
%
%Our main result is as follows.
%
%
%\begin{theorem}
%\label{thm:lbRegret}
%Under Assumptions \ref{as:bounded} and \ref{as:linear},
%$${\rm BayesRegret}(T, \pi^{\rm TS})  \leq d \sqrt{T \log\left(\frac{24 \sqrt{2 T}}{d} + 3\right)}.$$
%\end{theorem}
%\proof 
%Let $N(\C{S}, \epsilon, \|\cdot\|)$ denote the $\epsilon$-covering number of a set $\C{S}$ with respect to a norm $\|\cdot\|$. 
%Let $\t{\A} = \{a_1, a_2,\cdots,a_{|\t{\A}|}\}\subset\A$ be a set such that $$|\t{\A}|\leq N(\A, \e, \|\cdot\|_2),$$ and that
%\begin{equation}
%  \label{eq:cover}
%  \sup_{a\in\A}\min_{\t{a}\in\t{\A}} \|a - \t{a}\|_2\leq \e.
%\end{equation}
%Let $\B{B}(x,r)$ denote the closed Euclidean ball centered at $x$ with radius $r$.
%Consider a family of sets $\{\t{\A}_k\}_{k=1}^{|\t{\A}|}$, where $\t{\A}_1 = \B{B}(a_1, \e)\cap\A$ and
%\[
%  \t{\A}_k = \big( \B{B}(a_k, \e)\cap\A \big) \backslash \left( \bigcup_{j=1}^{k-1}\t{\A}_j \right),\ k=2,\cdots, |\t{\A}|.
%\]
%From \eqref{eq:cover}, we can see that $\{\t{\A}_k\}$ is a disjoint partition of $\A$. Let $\t{\C{M}} = \left\{ \t{p}_1, \cdots, \t{p}_{|\t{\A}|} \right\}$ be a subset of $\C{M}$ such that
%\[
%  \a^*(\t{p}_k) = a_k,\ k=1,2\cdots, |\t{\A}|.
%\]
%Consider a deterministic encoder, whose output $\psi^*:\C{M} \rightarrow \tilde{\C{M}}$ is given by
%\[
%  \psi^*(p) = \t{p}_k,\ \text{for all $p$ such that $\a^*(p)\in\t{\A}_k$}.
%\]
%Then, for all $p \in \C{M}$, we have
%\[
%  \|\alpha^*(p) - \alpha^*(\psi(p))\|_2\leq\e.
%\]
%Therefore there is,
%\begin{eqnarray*}
%\Big|\E\Big[R(Y_{t,\alpha^*(p)}) - R(Y_{t,\alpha^*(\psi(p))}) \big| p^* \Big]\Big|
%&=& \big|(p^*)^\top (\alpha^*(p) - \alpha^*(\psi(p)))\big| \\
%&\leq& \|p^*\|_2 \|\alpha^*(p) - \alpha^*(\psi(p))\|_2 \\
%&\leq& \epsilon.
%\end{eqnarray*}
%On the other hand, consider set $\t{\C{P}}_k = \{p\in\C{P}: \psi(p)=\t{p}_k\}$, we have that $\t{\C{P}}_k = \left[ (\a^*)^{-1}(\t{\A}_k)\right]\cap\C{P}$. Since, according to assumption \ref{as:measurable}, $\a^*$ is a measurable mapping from $\overline{\C{P}}$ to $\A$, and each $\t{\A}_k$ is Borel measurable, we have that each $\t{\C{P}}_k$ is also Borel measurable. This justifies the measurability of $\psi$, hence there is $\psi\in\Psi_\e$.
%
%
%Using a covering number bound from \cite{vandervaart1997weak}, we have
%$$N(\A, \epsilon, \|\cdot\|_2) \leq N(\mathcal{B}_d, \epsilon, \|\cdot\|_2) \leq \left( \frac{2}{\epsilon} + 1 \right)^d.$$
%Hence,
%$$\rho(\epsilon) = \inf_{\h{\psi} \in \Psi_\epsilon} H(\h{\psi}(\theta^*)) \leq H(\psi(\th^*)) \leq \log| \t{\Theta} | \leq d \log \left(  \frac{2}{\epsilon} + 1 \right).$$
%Letting $\epsilon = d/4 \sqrt{2 T}$, we obtain
%$$\rho\left( \frac{d}{4\sqrt{2T}} \right) \leq d \log\left(\frac{8\sqrt{2 T}}{d} + 1\right)$$
%and it follows from Theorems \ref{thm:general_bound} and \ref{thm:lbir} that
%\begin{eqnarray*}
%{\rm BayesRegret}(T, \pi^{\rm TS})  
%&\leq&  \inf_{\epsilon \geq 0} \left(\sqrt{\frac{d}{2}\cdot \rho\left( \e \right) T } + 4\epsilon T\right) \\
%&\leq& \sqrt{\frac{d T}{2}  \rho\left( \frac{d}{4\sqrt{2T}} \right) } + 4 \left(\frac{d}{4 \sqrt{2 T}}\right) T \\
%&\leq& d \sqrt{\frac{T}{2} \log\left(\frac{8 \sqrt{2 T}}{d} + 1\right)} + d \sqrt{\frac{T}{2}} \\
%&\leq& d \sqrt{T \log\left(\frac{24 \sqrt{2 T}}{d} + 3\right)},
%\end{eqnarray*}
%as desired. \qed

\bibliography{bibliography}
\bibliographystyle{plain}
\end{document}







 
















