\documentclass[11pt]{article}
\usepackage{caption}
\usepackage{enumitem}
\usepackage{color}
\captionsetup{font={scriptsize}}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage[top=0.75in, bottom=0.75in, left=0.9in, right=0.9in]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bm}
\usepackage[titletoc, title]{appendix}

\usepackage[colorlinks,
            linkcolor=black,
            anchorcolor=black,
            citecolor=black
            ]{hyperref}

% Theorem Environments
\newtheorem{theorem}{Theorem}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{assumption}{Assumption}
\newtheorem{definition}{Definition}
\newtheorem{fact}{Fact}
\newtheorem{remark}{Remark}
\newtheorem{example}{Example}


\DeclareMathOperator*{\argmax}{\arg\!\max}
\DeclareMathOperator*{\argmin}{\arg\!\min}

\renewenvironment{proof}{{\bfseries Proof}}

\def\th{\theta}
\def\Th{\Theta}
\def\s{\sigma}
\def\st{\star}
\def\a{\alpha}
\def\b{\beta}
\def\T{\top}
\def\d{\delta}
\def\D{\Delta}
\def\e{\epsilon}
\def\g{\gamma}
\def\l{\lambda}
\def\z{\zeta}
\def\O{\Omega}

\def\h#1{\hat{#1}}
\def\C#1{\mathcal{#1}}
\def\B#1{\mathbf{#1}}
\def\BB#1{\mathbb{#1}}
\def\BM#1{\bm{#1}}
\def\SF#1{\mathsf{#1}}
\def\RM#1{\mathrm{#1}}
\def\dkl#1#2{d\left( #1 \| #2 \right)}
\def\E#1{\mathbb{E}\left[ #1\right]}
\def\EE#1#2{\mathbb{E}_{#1}\left[ #2\right]}
\def\P#1{\mathbb{P}\left( #1\right)}
\def\PP#1#2{\mathbb{P}_{#1}\left( #2\right)}
\def\I#1#2{\mathbb{I}\left(#1;#2\right)}
\def\II#1#2#3{\mathbb{I}_#1\left(#2;#3\right)}
\def\H#1{\mathbb{H}\left( #1\right)}
\def\HH#1#2{\mathbb{HH}_{#1}\left( #2\right)}

% Macros specific to this document
\def\BMa{\BM{a}}
\def\BMA{\BM{A}}
\def\BMG{\BM{G}}
\def\hBMa{\h{\BM{a}}}
\def\hBMA{\h{\BM{A}}}
\def\hBMG{\h{\BM{G}}}
\def\hCA{\h{\C{A}}}
\def\hPi{\h{\Pi}}
\def\hpi{\h{\pi}}
\def\norm#1{\left\| #1 \right\|}	% norm
\def\normd#1{\left\| #1 \right\|_*} % dual norm
\def\FC{\mathfrak{C}}

\begin{document}
\title{Information-Theoretic Analysis of Thompson Sampling on Continuous Action Spaces (Formal)}
\maketitle
In this manuscript we focus on upper-bounding the regret of Thompson Sampling on linear bandit problems with continuous action spaces. Formally, let $\C{A}\subset\BB{R}^d$ be a compact set of actions and $T$ be a finite horizon. In each stage $t=1,2,\cdots,T$, the agent plays a (possibly random) action $\BM{A}_t\in\C{A}$ and receives a reward $R_t=\BM{A}_t^\T \BM{G}_*+Z_t$, where $\BM{G}_*$ is a {\it random} yet {\it static} underlying parameter in the parameter set $\C{G}\subset\BB{R}^d$ which we assume to be compact, and $Z_t\overset{\mathrm{i.i.d.}}{\sim}\SF{N}(0, \s^2)$ is a Gaussian noise term. The goal of the agent is to maximize the total reward up to stage $T$. We use {\it expected cumulative regret} to evaluate the performance of the agent, which is the expected difference between the maximum achievable reward and the reward received by the agent, defined by

\begin{equation}
\label{eq:1}
\SF{Reg}_T := \E{\sum_{t=1}^T R_*-R_t} = \E{\sum_{t=1}^T \BM{A}_*^\T \BM{G}_*-\BM{A}_t^\T \BM{G}_*},
\end{equation}
where $R_* = \max_{\BM{a}\in\C{A}}\BM{a}^\T \BM{G}_*$ and $\BM{A}_* = \argmax_{\BM{a}\in\C{A}}\BM{a}^\T \BM{G}_*$ are the maximum reward that can be received in one stage in average and the optimal action, respectively, and the expectation is taken over all possible action-reward sequences experienced by the agent up to time $T$. Throughout this manuscript, we use bold letters (e.g. $\BM{a}, \BM{A}$) to represent vectors and normal letters (e.g. $a, A$) scalars. All capital Roman letters are random variables in a common probability space $(\O, \C{F}, \BB{P})$, and lowercase Roman letters are instances of random variables.\par
The Thompson Sampling (TS) algorithm is described as follows. At the beginning the agent holds a prior belief towards the underlying parameter $\BM{G}_*$, which we denote by $\Pi_0$ with density $\pi_0$. At each stage $t=1,\cdots, T$, the agent samples a parameter $\BM{G}_t$ from the distribution $\Pi_{t-1}$ and plays the action $\BM{A}_t=\argmax_{\BM{a}\in\C{A}}\BM{a}^\T \BM{G}_t$. After receiving the reward $R_t$, the agent updates her belief towards $\BM{G}_*$ into $\Pi_t$ (with density $\pi_t$), and enters the next stage. We denote the expected cumulative regret of Thompson Sampling algorithm as $\SF{Reg}_T^{\RM{TS}}$. When $\C{A}$ (or equivalently $\C{G}$) is a finite set, an information-theoretic analysis in \cite{russo2016information} shows that 

\begin{equation}
\label{eq:2}
\SF{Reg}_T^{\RM{TS}} \leq \sqrt{\frac{dT}{2}\cdot\H{\Pi_0}},
\end{equation}
where $\H{\cdot}$ denotes the well-defined discrete information entropy. The main contribution of this manuscript is the generalization of this result to the case where $\left|\C{A}\right|$ is very large, even uncountably infinite.\par
The generalization is convenient under the framework of {\it rate-distortion theory}. We first define a distortion metric between two vectors $\bm{v}$ and $\bm{v}'$ in $\C{A}$:
\[
\SF{d}(\bm{v}, \bm{v}') := \max_{\bm{g}\in\C{G}} \left|\left(\bm{v} - \bm{v}'\right)^\T\bm{g} \right|.
\]
In addition, for two random variables $\bm{X}$ and $\bm{X}'$ taking values in $\C{A}$, the expected distortion is defined as
\[
\SF{D}(\bm{X}, \bm{X}') := \E{\SF{d}(\bm{X}, \bm{X}')}.
\]
It is worth noting that the distortion metrics $\SF{d}$ and $\SF{D}$ are both symmetric.\par
Based on the distortion metrics, we define an process running in parallel with the original TS algorithm, which we call the {\it distorted Thompson Sampling} (DTS) process. Let $\FC$ be an information channel that receives an input $\BM{X}_{\RM{i}}\in\C{A}$ and produces an output $\BM{X}_{\RM{o}}\in \hCA$, where $\hCA$ is a finite subset of $\C{A}$. The channel $\FC$ is chosen such that 
\begin{equation}
  \label{eq:3}
  \SF{D}(\BM{X}_{\RM{i}}, \BM{X}_{\RM{o}}) \leq \d,
\end{equation}
where $\d$ is a fixed positive number. At stage $t=1,2,\cdots$, TS sends the chosen action $\BMA_t$ to the input of channel $\FC$, which yields an output $\hBMA_t$. DTS then plays theaction $\hBMA_t$. The process is illustrated in Figure 1. Let $\h{R}_t$ be the reward received by DTS in stage $t$, we define the cumulative regret of DTS up to time horizon $T$ as
\begin{equation}
  \label{eq:4}
  \widehat{\SF{Reg}}_{T}^{\RM{TS}} := \sum_{t=1}^T \E{\h{R}_* - \h{R}_t},
\end{equation}
where $\h{R}_*$ is the maximum reward corresponding to $\hBMA_*$, the optimal action in DTS. Notice that we can decompose the cumulative regret of TS as following:
\begin{align}
  \SF{Reg}_{T}^{\RM{TS}}
  &= \sum_{t=1}^T \E{R_* - \h{R}_*}
  + \sum_{t=1}^T \E{\h{R}_t - R_t}
  + \sum_{t=1}^T \E{\h{R}_* - \h{R}_t} \label{eq:5}\\
  &= T\cdot \E{R_* - \h{R}_*}
  + \sum_{t=1}^T \E{\h{R}_t - R_t}
  + \widehat{\SF{Reg}}_{T}^{\RM{TS}} \label{eq:6}
\end{align}
In plain words, on the right-hand side the first term is the difference between the maximum reward in TS and DTS, the second term is the sum of the reward difference between TS and DTS in each stage, and the last term is the cumulative regret of DTS. In the following, all three terms are upper bounded respectively.\par
Notice that $\h{R}_* = \hBMA_*^\T \BMG_*$, the first term can be bounded by
\begin{align}
  \E{R_* - \h{R}_*}
  &= \E{\BMA_*^\T\BMG_* - \hBMA^\T \BMG_*} \label{eq:7}\\
  &\leq  \E{\left( \BMA_*- \hBMA_*\right)^\T\BMG_*} \\
  &\leq \SF{D} (\BMA_*, {\hBMA}_*)\\
  &\leq \d \label{eq:10},
\end{align}
where \eqref{eq:10} comes from the definition of channel $\FC$. The second term can be similarly bounded:
\begin{align}
  \label{eq:11}
  \left| \E{\h{R}_t - R_t} \right|
  = \left| \E{\left( \hBMA_t - \BMA_t \right)^\T \BMG_*} \right|
  \leq \SF{D}(\hBMA_t, \BMA_t)\leq \d. 
\end{align}\par
Finally we turn to bound the cumulative regret of DTS. It is worth noting that the action-reward sequence of DTS, which is $\left(\hBMA_1, \h{R}_1, \cdots, \hBMA_T, \h{R}_T\right)$, does {\it not} come from a standard Thompson Sampling algorithm, since the marginal distribution of $\hBMA_{t+1}$ is not the posterior of $\hBMA_*$ after we observe $\hBMA_1, \h{R}_1, \cdots, \hBMA_t, \h{R}_t$. Therefore we are not warranted to use the existing regret bounds of discrete Thompson Sampling. In order to bypass this inconvenience, we can decompose DTS into multiple one-stage Thompson Sampling processes. Let $\h{\Pi}_{t-1}$ be the marginal distribution of $\hBMA_t$, the action played by DTS at stage $t$. By definition, $\h{\Pi}_{t-1}$ is the channel-corrupted version of $\Pi_{t-1}$. In stage $t$, DTS samples $\hBMA_t$ from $\h{\Pi}_{t-1}$, and receives reward $\h{R}_t$. This can be viewed as a one-stage Thompson Sampling on discrete action space $\hCA$ with prior $\h{\Pi}_{t-1}$. We can thereby apply the one-stage regret bound derived from information ratio by Russo and Van Roy in \cite{russo2016information}:
\begin{equation}
\label{eq:12}
\EE{t}{R^* - \h{R}_t} \leq \sqrt{\frac{d}{2}\cdot \II{t}{\hBMA_*}{(\hBMA_t, \h{R}_t)}},
\end{equation}
where  $\I{\cdot}{\cdot}$ is the mutual information, and the subscript $t$ denotes conditioning on the sequence $(\BMA_1, R_1,\cdots \BMA_{t-1}, R_{t-1})$ yielded by the TS, and should be interpreted as
\begin{equation}
\label{eq:13}
\EE{t}{\ \cdot\ } = \E{\ \cdot\ \big| \BMA_1=\BMa_1, R_1=r_1, \cdots, \BMA_{t-1}=\BMa_{t-1}, R_{t-1}=r_{t-1}},
\end{equation}
where $\{\BMa_j\}$ and $\{r_j\}$ are instantiations of random variables $\{\BMA_j\}$ and $\{R_j\}$. The same interpretation extends to $\II{t}{\cdot}{\cdot}$. \par
From the tower property of expectation we have
\begin{align}
\widehat{\mathsf{Reg}}^{\RM{TS}}_T
&= \E{\sum_{t=1}^T \h{R}_*-\h{R}_t } \label{eq:14}\\
&= \sum_{t=1}^T \E{\E{\h{R}_*-\h{R}_t \Big| \BMA_1, R_1,\cdots \BMA_{t-1}, R_{t-1}}} \label{eq:15}\\
&= \sum_{t=1}^T \E{\EE{t}{\h{R}_*-\h{R}_t}} \label{eq:16}\\
&\leq \sum_{t=1}^T \E{\sqrt{\frac{d}{2}\cdot \II{t}{\hBMA_*}{(\hBMA_t, \h{R}_t)}}} \label{eq:17}\\
&\leq \sum_{t=1}^T \sqrt{\frac{d}{2}\cdot \E{\II{t}{\hBMA_*}{(\hBMA_t, \h{R}_t)}}} \label{eq:18}\\
&= \sum_{t=1}^T \sqrt{\frac{d}{2}\cdot \I{\hBMA_*}{(\hBMA_t, \h{R}_t)\Big| (\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})}} \label{eq:19},
\end{align}
where \eqref{eq:18} follows from Jensen's inequality. Notice that conditioned on $(\BMA_1, R_1, \cdots, \BMA_t, R_t)$, the distribution of $\hBMA_*$ is the distribution of $\BMA_*$ distorted by channel $\FC$, which is independent of $(\hBMA_t, \h{R}_t)$. By applying data processing inequality we should have
\begin{equation}
  \label{eq:20}
  \I{\hBMA_*}{(\hBMA_t, \h{R}_t)\big| (\BMA_1, R_1, \cdots, \BMA_{t-1}, R_{t-1})} \leq \I{\hBMA_*}{(\BMA_t, R_t) \big| (\BMA_1, R_1, \cdots, \BMA_{t-1}, R_{t-1})}.
\end{equation}
This leads to
\begin{align}
\widehat{\mathsf{Reg}}^{\RM{TS}}_T
&\leq \sum_{t=1}^T \sqrt{\frac{d}{2}\cdot \I{\hBMA_*}{(\hBMA_t, \h{R}_t)\Big| (\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})}} \label{eq:21}\\
&\leq \left(\frac{dT}{2}\sum_{t=1}^T \I{\hBMA_*}{(\hBMA_t, \h{R}_t)\Big| (\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})} \right)^{\frac{1}{2}} \label{eq:22}\\
&\leq \left(\frac{dT}{2}\sum_{t=1}^T \I{\hBMA_*}{(\BMA_t, R_t)\Big| (\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})} \right)^{\frac{1}{2}} \label{eq:23}\\
&= \left(\frac{dT}{2}\sum_{t=1}^T \I{\hBMA_*}{(\BMA_1, R_1,\cdots, \BMA_{t}, R_{t})} - \I{\hBMA_*}{(\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})} \right)^{\frac{1}{2}} \label{eq:24}\\
&= \sqrt{\frac{dT}{2}\I{\hBMA_*}{(\BMA_1, R_1,\cdots, \BMA_{T}, R_{T})}}, \label{eq:25}\\
&\leq \sqrt{\frac{dT}{2}\I{\hBMA_*}{\BMA_*}} \label{eq:26}
\end{align}
where \eqref{eq:22} results from Cauchy-Schwartz inequality, and \eqref{eq:26} results from data processing inequality (since $(\BMA_1, R_1,\cdots, \BMA_T, R_T)\to \BMA_* \to \hBMA_*$ is a Markov chain).\par
By plugging in \eqref{eq:10}, \eqref{eq:11} and \eqref{eq:26} into \eqref{eq:6}, we can see that for fixed $\d>0$ and channel $\FC$, there is
\begin{equation}
  \label{eq:27}
  \SF{Reg}_{T}^{\RM{TS}}(\d, \FC) \leq 2\d T + \sqrt{\frac{dT}{2}\I{\hBMA_*}{\BMA_*}},
\end{equation}
where we should keep in mind that the random variable $\hBMA_*$ depends on channel $\FC$. In order to obtain the tightest upper bound for $\SF{Reg}_{T}^{\RM{TS}}$, we would want to find the infimum of the right-hand side of \eqref{eq:27} over all possible $\d$ and $\FC$. Let $\SF{R}_{\SF{D}}$ be the rate-distortion function of channel $\FC$ corresponding to the input $\BMA_*$, the standard rate-distortion theory result in $\S 10.5$ of \cite{cover2012elements} shows that for fixed $\d>0$ and every $\e>0$, there exists a channel $\FC$ such that $\I{\hBMA_*}{\BMA_*} \leq \SF{R}_{\SF{D}}(\d)+\e$. Therefore by taking the infimum over all channel $\FC$ with distortion not exceeding $\d$, we arrive at the upper bound
\begin{equation}
  \label{eq:28}
  \SF{Reg}_T^{\RM{TS}}\leq \inf_{\d>0} \left\{ 2\d T + \sqrt{\frac{dT}{2}\SF{R}_{\SF{D}}(\d)} \right\}.
\end{equation}
\bibliography{bibliography}
\bibliographystyle{plain}
\end{document}






















