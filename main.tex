\documentclass[11pt]{article}
\usepackage{caption}
\usepackage{enumitem}
\usepackage{color}
\captionsetup{font={scriptsize}}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage[top=0.75in, bottom=0.75in, left=0.9in, right=0.9in]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bm}
\usepackage[titletoc, title]{appendix}

\usepackage[colorlinks,
            linkcolor=black,
            anchorcolor=black,
            citecolor=black
            ]{hyperref}

% Theorem Environments
\newtheorem{theorem}{Theorem}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{assumption}{Assumption}
\newtheorem{definition}{Definition}
\newtheorem{fact}{Fact}
\newtheorem{remark}{Remark}
\newtheorem{example}{Example}


\DeclareMathOperator*{\argmax}{\arg\!\max}
\DeclareMathOperator*{\argmin}{\arg\!\min}

\renewenvironment{proof}{{\bfseries Proof}}

\def\th{\theta}
\def\Th{\Theta}
\def\s{\sigma}
\def\S{\Sigma}
\def\st{\star}
\def\a{\alpha}
\def\b{\beta}
\def\T{\top}
\def\d{\delta}
\def\D{\Delta}
\def\e{\epsilon}
\def\g{\gamma}
\def\l{\lambda}
\def\z{\zeta}
\def\O{\Omega}

\def\h#1{\hat{#1}}
\def\C#1{\mathcal{#1}}
\def\B#1{\mathbf{#1}}
\def\BB#1{\mathbb{#1}}
\def\BM#1{\bm{#1}}
\def\SF#1{\mathsf{#1}}
\def\RM#1{\mathrm{#1}}
\def\dkl#1#2{d\left( #1 \| #2 \right)}
\def\E#1{\mathbb{E}\left[ #1\right]}
\def\EE#1#2{\mathbb{E}_{#1}\left[ #2\right]}
\def\P#1{\mathbb{P}\left( #1\right)}
\def\PP#1#2{\mathbb{P}_{#1}\left( #2\right)}
\def\I#1#2{\mathbb{I}\left(#1;#2\right)}
\def\II#1#2#3{\mathbb{I}_#1\left(#2;#3\right)}
\def\H#1{\mathbb{H}\left( #1\right)}
\def\HH#1#2{\mathbb{HH}_{#1}\left( #2\right)}

% Macros specific to this document
\def\BMa{\BM{a}}
\def\BMA{\BM{A}}
\def\BMG{\BM{G}}
\def\hBMa{\h{\BM{a}}}
\def\hBMA{\h{\BM{A}}}
\def\hBMG{\h{\BM{G}}}
\def\hCA{\h{\C{A}}}
\def\hPi{\h{\Pi}}
\def\hpi{\h{\pi}}
\def\norm#1{\left\| #1 \right\|}	% norm
\def\normd#1{\left\| #1 \right\|_*} % dual norm

\begin{document}
\title{Information-Theoretic Analysis of Thompson Sampling on Continuous Action Spaces (Formal)}
\maketitle
In this manuscript we focus on upper-bounding the regret of Thompson Sampling on linear bandit problems with continuous action spaces. Formally, let $\C{A}\subset\BB{R}^d$ be a compact set of actions and $T$ be a finite horizon. In each stage $t=1,2,\cdots,T$, the agent plays a (possibly random) action $\BM{A}_t\in\C{A}$ and receives a reward $R_t=\BM{A}_t^\T \BM{G}_*+Z_t$, where $\BM{G}_*$ is a {\it random} yet {\it static} underlying parameter in the parameter set $\C{G}\subset\BB{R}^d$, and $Z_t\overset{\mathrm{i.i.d.}}{\sim}\SF{N}(0, \s^2)$ is a Gaussian noise term. The goal of the agent is to maximize the total reward up to stage $T$. We use {\it expected cumulative regret} to evaluate the performance of the agent, which is the expected difference between the maximum achievable reward and the reward received by the agent, defined by

\begin{equation}
\label{eq:1}
\SF{Reg}_T := \E{\sum_{t=1}^T R_*-R_t} = \E{\sum_{t=1}^T \BM{A}_*^\T \BM{G}_*-\BM{A}_t^\T \BM{G}_*},
\end{equation}
where $R_* = \max_{\BM{a}\in\C{A}}\BM{a}^\T \BM{G}_*$ and $\BM{A}_* = \argmax_{\BM{a}\in\C{A}}\BM{a}^\T \BM{G}_*$ are the maximum reward that can be received in one stage in average and the optimal action, respectively, and the expectation is taken over all possible action-reward sequences experienced by the agent up to time $T$. Throughout this manuscript, we use bold letters (e.g. $\BM{a}, \BM{A}$) to represent vectors and normal letters (e.g. $a, A$) scalars. All capital Roman letters are random variables in a common probability space $(\O, \C{F}, \BB{P})$, and lowercase Roman letters are instances of random variables.\par
The Thompson Sampling (TS) algorithm is described as follows. At the beginning the agent holds a prior belief towards the underlying parameter $\BM{G}_*$, which we denote by $\Pi_0$ with density $\pi_0$. At each stage $t=1,\cdots, T$, the agent samples a parameter $\BM{G}_t$ from the distribution $\Pi_{t-1}$ and plays the action $\BM{A}_t=\argmax_{\BM{a}\in\C{A}}\BM{a}^\T \BM{G}_t$. After receiving the reward $R_t$, the agent updates her belief towards $\BM{G}_*$ into $\Pi_t$ (with density $\pi_t$), and enters the next stage. We denote the expected cumulative regret of Thompson Sampling algorithm as $\SF{Reg}_T^{\RM{TS}}$. When $\C{A}$ (or equivalently $\C{G}$) is a finite set, an information-theoretic analysis in \cite{russo2016information} shows that 

\begin{equation}
\label{eq:2}
\SF{Reg}_T^{\RM{TS}} \leq \sqrt{\frac{dT}{2}\cdot\H{\Pi_0}},
\end{equation}
where $\H{\cdot}$ denotes the well-defined discrete information entropy. The main contribution of this manuscript is the generalization of this result to the case where $\left|\C{A}\right|$ is very large, even uncountably infinite.\par
First, we define an algorithm parallel to TS, which we call the {\it discrete pseudo-Thompson Sampling} (DPTS) algorithm. For a fixed $\e>0$, let $\h{\C{A}} = \{\hBMa_1, \hBMa_2,\cdots, \hBMa_N\}\subseteq\C{A}$ be a finite $\e$-covering of $\C{A}$ with respect to the $\norm{\cdot}$ norm. $\h{\C{A}}$ naturally induces a disjoint partition $\C{A}=\bigcup_{j=1}^N\h{\C{A}}_j$ such that $\hBMa_j\in\h{\C{A}}_j$ and $\RM{diam}_{\norm{\cdot}}(\hCA_j)\leq 2\e$ for all $j=1,\cdots,N$. The algorithm DPTS runs in parallel with TS. At each stage $t=1,2,\cdots, T$, DPTS plays $\hBMA_t=\hBMa_k$, where $k$ is the unique index such that $\BMA_t\in\hCA_k$, and $\BMA_t$ is the action played by TS in stage $t$. It is easy to see that $\hBMA_t$ is unambiguously determined by $\BMA_t$, and we denote this mapping by $\hBMA_t=f(\BMA_t)$. More generally, for any $\BMa\in\C{A}$, if $\BMa\in\hCA_k$, then $f(\BMa)$ returns $\hBMa_k$. Hence there should be $\norm{\BMa - f(\BMa)}\leq 2\e$ for all $\BMa\in\C{A}$. Let the distribution of $\hBMA_t$ be $\hPi_{t-1}$ with probability mass $\hpi_{t-1}$. Note that both $\Pi_t$ and $\hPi_t$ depend on the action-reward sequence in TS, namely $\left( \BMA_1, R_1, \cdots, \BMA_{t}, R_t\right)$, and there is $\hpi_t(\hBMa_j) = \int_{\BMa\in\hCA_j}\RM{d}\pi_t$ (the probability assigned to $\hCA_j$ in TS is concentrated on $\hBMa_j$ in DPTS). The expected cumulative regret of DPTS is defined as

\begin{equation}
\label{eq:3}
\widehat{\SF{Reg}}^{\RM{TS}}_T := \E{\sum_{t=1}^T \h{R}_*-\h{R}_t},
\end{equation}
where, different from the regret definition in TS, $\h{R}_* = \max_{\hBMa\in\hCA} \hBMa^\T \BM{G}_*$ is the maximum achievable reward with respect to the finite set $\hCA$, as opposed to $\C{A}$. \par

Our argument is structured as follows:
\begin{enumerate}[label=(\alph*),leftmargin=2\parindent]
\item \label{en:1} showing that the difference between the expected rewards of TS and DPTS in each stage $d_t=\left| \E{R_t} - \E{\h{R}_t} \right|$ is small;
\item \label{en:2} showing that the difference between the maximum rewards in TS and DPTS $D=\E{R_* - \h{R}_*}$ is small (this term is always non-negative since $\hCA\subseteq \C{A}$);
\item \label{en:3} bounding the total regret $\widehat{\SF{Reg}}^{\RM{TS}}_T$.
\end{enumerate}
From the definitions there is 
\begin{equation}
\label{eq:4}
\SF{Reg}_T^{\RM{TS}} \leq D\cdot T + \widehat{\SF{Reg}}^{\RM{TS}}_T + \sum_{t=1}^T d_t, 
\end{equation}
Hence the above three steps (a)-(c) will effectively bound $\SF{Reg}_T^{\RM{TS}}$ as we desire.\par

To show \ref{en:1}, notice that
\begin{align}
d_t =\left| \E{R_t} - \E{\h{R}_t} \right|
&= \left| \E{\E{R_t-\h{R}_t}\big| \BMG_*} \right| \label{eq:5}\\
&= \left| \E{\sum_{j=1}^N \left( \int_{\BMa\in \hCA_j} \BMa^\T\BMG_* \RM{d}\pi_t - \hBMa_j^\T\BMG_*\cdot\hpi_t(\hBMa_j)\right) } \right|\label{eq:6}\\
&\leq \E{\sum_{j=1}^N \left| \int_{\BMa\in \hCA_j} \BMa^\T\BMG_* \RM{d}\pi_t - \hBMa_j^\T\BMG_*\cdot\hpi_t(\hBMa_j) \right|} \label{eq:7}\\
&= \E{\sum_{j=1}^N \left| \int_{\BMa\in \hCA_j} \left(\BMa - \hBMa_j\right)^\T\BMG_* \mathrm{d}\pi_t\right|} \label{eq:8}\\
&\leq \E{\sum_{j=1}^N \int_{\BMa\in \hCA_j} \norm{\BMa - \hBMa_j}\normd{\BMG_*} \mathrm{d}\pi_t}  \label{eq:9}\\
&\leq \E{2\e\normd{\BMG_*}\cdot \sum_{j=1}^N \pi(\hCA_j)}\label{eq:10}\\
&= 2\e\E{\normd{\BMG_*}}, \label{eq:11}
\end{align}
where $\normd{\cdot}$ is the dual norm of $\norm{\cdot}$, \eqref{eq:5} comes from the tower property of expectation, and \eqref{eq:11} follows from the fact that $\sum_{j=1}^N \pi(\hCA_j) = 1$.\par

A similar technique would justify \ref{en:2}. We have that
\begin{align}
D = \E{R_* - \h{R}_*}
&= \E{\E{R_*-\h{R}_* \Big| \BMG_*}} \label{eq:12} \\
&= \E{ \max_{\BMa\in\C{A}} \BMa^\T\BMG_* - \max_{\hBMa\in\hCA}\hBMa^\T\BMG_*} \label{eq:13}\\
&\leq \E{ \max_{\BMa\in\C{A}} \left(\BMa^\T\BMG_* - f(\BMa)^\T\BMG_*\right)} \label{eq:14}\\
&\leq \E{\max_{\BMa\in\C{A}} \norm{\BMa - f(\BMa)} \normd{\BMG_*}} \label{eq:15}\\
&\leq 2\e\E{\normd{\BMG_*}}, \label{eq:16}
\end{align}
where \eqref{eq:14} comes from the fact that $f(\BMa)\in\hCA$, which leads to $f(\BMa)^\T\BMG_* \leq \max_{\hBMa\in\hCA}\hBMa^\T\BMG_*$.\par

Finally we turn to prove \ref{en:3}. It is worth noting that the action-reward sequence of DPTS, which is $\left(\hBMA_1, \h{R}_1, \cdots, \hBMA_T, \h{R}_T\right)$, does {\it not} come from a standard Thompson Sampling algorithm, since $\hpi_{t+1}$ is not the posterior of $\BMG_*$ after we observe $\hBMA_1, \h{R}_1, \cdots, \hBMA_t, \h{R}_t$. Therefore we are not warranted to use the existing regret bounds of discrete Thompson Sampling. However, if we focus on one stage and look marginally at DPTS, that is, drawing $\hBMA_t$ from $\hpi_{t-1}$ and observing $\h{R}_t$, we can apply the one-step regret bound derived from information ratio by Russo and Van Roy:
\begin{equation}
\label{eq:17}
\EE{t}{R^* - \h{R}_t} \leq \sqrt{\frac{d}{2}\cdot \II{t}{\hBMA_*}{(\hBMA_t, \h{R}_t)}},
\end{equation}
where $\hBMA_*$ is the optimal action in $\hCA$, $\I{\cdot}{\cdot}$ is the mutual information, and the subscript $t$ denotes conditioning on the sequence $(\BMA_1, R_1,\cdots \BMA_{t-1}, R_{t-1})$ yielded by the TS, and should be interpreted as
\begin{equation}
\label{eq:18}
\EE{t}{\ \cdot\ } = \E{\ \cdot\ \big| \BMA_1=\BMa_1, R_1=r_1, \cdots, \BMA_{t-1}=\BMa_{t-1}, R_{t-1}=r_{t-1}},
\end{equation}
where $\{\BMa_j\}$ and $\{r_j\}$ are instantiations of random variables $\{\BMA_j\}$ and $\{R_j\}$. The same interpretation extends to $\II{t}{\cdot}{\cdot}$. From the tower property of expectation we have
\begin{align}
\widehat{\mathsf{Reg}}^{\RM{TS}}_T
&= \E{\sum_{t=1}^T \h{R}_*-\h{R}_t } \label{eq:19}\\
&= \sum_{t=1}^T \E{\E{\h{R}_*-\h{R}_t \Big| \BMA_1, R_1,\cdots \BMA_{t-1}, R_{t-1}}} \label{eq:20}\\
&= \sum_{t=1}^T \E{\EE{t}{\h{R}_*-\h{R}_t}} \label{eq:21}\\
&\leq \sum_{t=1}^T \E{\sqrt{\frac{d}{2}\cdot \II{t}{\hBMA_*}{(\hBMA_t, \h{R}_t)}}} \label{eq:22}\\
&\leq \sum_{t=1}^T \sqrt{\frac{d}{2}\cdot \E{\II{t}{\hBMA_*}{(\hBMA_t, \h{R}_t)}}} \label{eq:23}\\
&= \sum_{t=1}^T \sqrt{\frac{d}{2}\cdot \I{\hBMA_*}{(\hBMA_t, \h{R}_t)\Big| (\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})}} \label{eq:24},
\end{align}
where \eqref{eq:23} follows from Jensen's inequality. From the definition of DPTS, $\hBMA_t=f(\BMA_t)$. Further, under the following coupling relation
\begin{equation}
\h{R}_t = R_t - \BMA_t^\T\BMG_* + \hBMA_t^\T\BMG_*,
\end{equation}
we can see that the marginal distribution of $\h{R}_t$ is identical to $\hBMA_t^\T\BMG_* + Z_t$, where $Z_t\sim\SF{N}(0,\s^2)$ is the Gaussian noise. Therefore $\h{R}_t$ can be seen as a function of $R_t$. With these in mind, we have
\begin{align}
\widehat{\mathsf{Reg}}^{\RM{TS}}_T
&\leq \sum_{t=1}^T \sqrt{\frac{d}{2}\cdot \I{\hBMA_*}{(\hBMA_t, \h{R}_t)\Big| (\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})}} \label{eq:26}\\
&\leq \left(\frac{dT}{2}\sum_{t=1}^T \I{\hBMA_*}{(\hBMA_t, \h{R}_t)\Big| (\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})} \right)^{\frac{1}{2}} \label{eq:27}\\
&\leq \left(\frac{dT}{2}\sum_{t=1}^T \I{\hBMA_*}{(\BMA_t, R_t)\Big| (\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})} \right)^{\frac{1}{2}} \label{eq:28}\\
&\leq \left(\frac{dT}{2}\sum_{t=1}^T \I{\hBMA_*}{(\BMA_1, R_1,\cdots, \BMA_{t}, R_{t})} - \I{\hBMA_*}{(\BMA_1, R_1,\cdots, \BMA_{t-1}, R_{t-1})} \right)^{\frac{1}{2}} \label{eq:29}\\
&= \sqrt{\frac{dT}{2}\I{\hBMA_*}{(\BMA_1, R_1,\cdots, \BMA_{T}, R_{T})}}, \label{eq:30}
\end{align}
where \eqref{eq:27} follows from Jensen's inequality and \eqref{eq:28} comes from Data Processing inequality. Plugging \eqref{eq:11}, \eqref{eq:16} and \eqref{eq:30} into \eqref{eq:4}, we arrive at
\begin{equation}
\label{eq:31}
\SF{Reg}_T^{\RM{TS}} \leq 4\e\E{\normd{\BMG_*}}\cdot T + \sqrt{\frac{dT}{2}\I{\hBMA_*}{(\BMA_1, R_1,\cdots, \BMA_{T}, R_{T})}}.
\end{equation}
To obtain the tightest bound, we can choose $\e$ and the definition of norm $\norm{\cdot}$ such that the right-hand side of \eqref{eq:31} is minimized.


\bibliography{bibliography}
\bibliographystyle{plain}
\end{document}






















